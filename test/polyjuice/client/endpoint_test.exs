# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.EndpointTest do
  use ExUnit.Case

  test "parse response" do
    # just test the error handling -- successful responses will be checked by
    # the various endpoint tests

    # the server returned a Matrix error
    assert Polyjuice.Client.Endpoint.parse_response(
             %{},
             404,
             [{"Content-Type", "application/json"}],
             ~s({"errcode": "M_NOT_FOUND", "error": "not found"})
           ) == {:error, 404, %{"errcode" => "M_NOT_FOUND", "error" => "not found"}}

    # the server claims to have succeeded, but the body is not JSON
    assert Polyjuice.Client.Endpoint.parse_response(%{}, 200, [], "not JSON") ==
             {:error, 500,
              %{"errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE", "body" => "not JSON"}}

    # the server returned a non-Matrix error
    assert Polyjuice.Client.Endpoint.parse_response(%{}, 404, [], "Not found") ==
             {:error, 404,
              %{"errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE", "body" => "Not found"}}
  end
end
