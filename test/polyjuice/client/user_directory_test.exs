# Copyright 2020 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.UserDirectoryTest do
  use ExUnit.Case

  # Not using doctest as it's meant for pure functions where all arguments can be literals (i.e. not the case because of argument client)
  # doctest Polyjuice.Client.UserDirectory

  test "search user with limit" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostUserDirectorySearch{
               search_term: "alice",
               limit: 10
             },
             {
               :ok,
               {[
                  %{
                    "avatar_url" => "mxc://bar.com/foo",
                    "display_name" => "Alice",
                    "user_id" => "@alice:example.com"
                  }
                ], limited: false}
             }
           }
         } do
      {:ok, search_results} =
        Polyjuice.Client.UserDirectory.search_user(
          client,
          "alice",
          10
        )

      assert search_results ==
               {[
                  %{
                    "avatar_url" => "mxc://bar.com/foo",
                    "display_name" => "Alice",
                    "user_id" => "@alice:example.com"
                  }
                ], limited: false}
    end
  end

  test "search user without limit" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostUserDirectorySearch{
               search_term: "alice",
               limit: nil
             },
             {
               :ok,
               {
                 [
                   %{
                     "avatar_url" => "mxc://bar.com/foo",
                     "display_name" => "Alice",
                     "user_id" => "@alice:example.com"
                   }
                 ],
                 limited: false
               }
             }
           }
         } do
      {:ok, search_results} =
        Polyjuice.Client.UserDirectory.search_user(
          client,
          "alice",
          nil
        )

      assert search_results ==
               {
                 [
                   %{
                     "avatar_url" => "mxc://bar.com/foo",
                     "display_name" => "Alice",
                     "user_id" => "@alice:example.com"
                   }
                 ],
                 limited: false
               }
    end
  end
end
