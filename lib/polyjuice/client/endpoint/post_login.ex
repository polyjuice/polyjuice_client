# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostLogin do
  @moduledoc """
  Log in a user

  https://matrix.org/docs/spec/client_server/r0.5.0#post-matrix-client-r0-login
  """

  @type t :: %__MODULE__{
          type: String.t(),
          identifier: map,
          password: String.t() | nil,
          token: String.t() | nil,
          device_id: String.t() | nil,
          initial_device_display_name: String.t() | nil
        }

  @enforce_keys [:type, :identifier]

  defstruct [
    :type,
    :identifier,
    :password,
    :token,
    :device_id,
    :initial_device_display_name
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PostLogin{
          type: type,
          identifier: identifier,
          password: password,
          token: token,
          device_id: device_id,
          initial_device_display_name: initial_device_display_name
        }) do
      body =
        [
          [{"type", type}, {"identifier", identifier}],
          if(password != nil, do: [{"password", password}], else: []),
          if(token != nil, do: [{"token", token}], else: []),
          if(device_id != nil, do: [{"device_id", device_id}], else: []),
          if(initial_device_display_name != nil,
            do: [{"initial_device_display_name", initial_device_display_name}],
            else: []
          )
        ]
        |> Enum.concat()
        |> Map.new()
        |> Jason.encode_to_iodata!()

      Polyjuice.Client.Endpoint.HttpSpec.post(
        :r0,
        "login",
        body: body,
        auth_required: false
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end
end
