# Copyright 2021 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Attachment do
  @moduledoc """
  Attachment related functions.

  Use the `new` function to create an attachment.  The attachment type can be
  set by using the `as_*` functions.  For example:

      Polyjuice.Client.Room.send_message(
        client,
        "!roomid",
        Polyjuice.Client.Attachment.new(
          client,
          {:file, "file.png"},
          mimetype: "image/png"
        )
        # mark it as image, and set the size
        |> Polyjuice.Client.Attachment.as_image(width: 250, height: 250)
        |> Polyjuice.Client.Attachment.add_thumbnail(
          client,
          {:file, "thumbnail.png"},
          "image/png",
          width: 10,
          height: 10
        )
      )

  """

  @typedoc """
  Represents an attachment message.

  The map will have the following keys: `"msgtype"` (a string), `"body"` (a
  string), and `"info"` (a map).

  """
  @type attachment() :: map

  @doc """
  Create an attachment object.

  The attachment data can be provided as:
  - a tuple of the form `{:data, file_contents, filename}`, which will use
    `file_contents` as the file;
  - a tuple of the form `{:file, filename}`, which will upload a local file; or
  - a tuple of the form `{:url, mxc_url}`, which uses an already-uploaded file.

  Supported options are:
  - `body:` the message body.  Defaults to the filename, if provided, or
    "Attachment" if the data is of the form `{:url, mxc_url}`.
  - `mimetype:` the mimetype for the attachment.  Defaults to
    `application/octet-stream`.
  - `size:` the size in bytes of the file.  If not specified, it will attempt to
    determine the size automatically.
  - `info:` metadata about the image, in the format specified by the `info`
    property of the `m.file` (or similar) msgtype
  - `msgtype:` the `msgtype` to use for the message.  Defaults to `m.file`.
    This can also be set by using one of the `as_*` functions (e.g. `as_image`
    will change the message to an `m.image` msgtype) rather than setting the
    `msgtype` here.
  """
  @spec new(
          client_api :: Polyjuice.Client.API.t(),
          data ::
            {:data, binary, String.t()}
            | {:file, String.t()}
            | {:url, String.t() | URI.t()},
          opts :: Keyword.t()
        ) :: attachment()
  def new(
        client_api,
        data,
        opts \\ []
      )
      when is_tuple(data) do
    mimetype = Keyword.get(opts, :mimetype, "application/octet-stream")

    info =
      Keyword.get(opts, :info, %{})
      |> Map.put("mimetype", mimetype)
      |> set_info_key("size", Keyword.get_lazy(opts, :size, fn -> get_data_size(data) end))

    %{
      "msgtype" => Keyword.get(opts, :msgtype, "m.file"),
      "body" => Keyword.get_lazy(opts, :body, fn -> get_filename(data) end),
      "info" => info
    }
    |> build_attachment_data(client_api, mimetype, data)
  end

  @doc """
  Transform a attachment object object into an attachment image object

  Supported options are:
  - `width:` the intended display width of the image
  - `height:` the intended display height of the image
  """
  @spec as_image(
          msg :: attachment(),
          opts :: Keyword.t()
        ) :: attachment()
  def as_image(msg, opts \\ []) when is_map(msg) and is_list(opts) do
    info =
      Map.get(msg, "info")
      |> set_info_key("w", Keyword.get(opts, :width))
      |> set_info_key("h", Keyword.get(opts, :height))

    Map.put(msg, "msgtype", "m.image") |> Map.put("info", info)
  end

  @doc """
  Transform a attachment object object into an attachment file object.
  """
  @spec as_file(
          msg :: attachment(),
          filename :: String.t()
        ) :: attachment()
  def as_file(msg, filename) when is_binary(filename) do
    info =
      Map.get(msg, "info")
      |> Map.put("filename", filename)

    Map.put(msg, "msgtype", "m.file") |> Map.put("info", info)
  end

  @doc """
  Transform a attachment object object into an attachment audio object.

  The `duration` is given in milliseconds.
  """
  @spec as_audio(
          msg :: attachment(),
          duration :: integer | nil
        ) :: attachment()
  def as_audio(msg, duration \\ nil)
      when is_map(msg) or (is_integer(duration) or duration == nil) do
    info =
      Map.get(msg, "info")
      |> set_info_key("duration", duration)

    Map.put(msg, "msgtype", "m.audio") |> Map.put("info", info)
  end

  @doc """
  Transform a attachment object object into an attachment video object.

  Supported options:
  - `width:` the display width of the video
  - `height:` the display height of the video
  - `duration:` the duration of the video in milliseconds
  """
  @spec as_video(
          msg :: attachment(),
          opts :: Keyword.t()
        ) :: attachment()
  def as_video(msg, opts \\ [])
      when is_map(msg) and is_list(opts) do
    info =
      Map.get(msg, "info")
      |> set_info_key("w", Keyword.get(opts, :width))
      |> set_info_key("h", Keyword.get(opts, :height))
      |> set_info_key("duration", Keyword.get(opts, :duration))

    Map.put(msg, "msgtype", "m.video") |> Map.put("info", info)
  end

  @doc """
  Add a thumbnail to an attachment object.

  Supported options are:
  - `height:` the thumbnail height
  - `width:` the thumbnail width
  - `size:` the size in bytes of the thumbnail
  """
  @spec add_thumbnail(
          msg :: attachment(),
          client_api :: Polyjuice.Client.API.t() | nil,
          data ::
            {:data, binary, String.t()} | {:file, String.t()} | {:url, String.t() | URI.t()},
          mimetype :: String.t(),
          opts :: Keyword.t()
        ) :: attachment()
  def add_thumbnail(msg, client_api, data, mimetype, opts \\ [])
      when is_map(msg) and is_binary(mimetype) and is_list(opts) do
    thumbnail_info =
      %{"mimetype" => mimetype}
      |> set_info_key("h", Keyword.get(opts, :height))
      |> set_info_key("w", Keyword.get(opts, :width))
      |> set_info_key("size", Keyword.get_lazy(opts, :size, fn -> get_data_size(data) end))

    info =
      Map.get(msg, "info")
      |> build_attachment_data(client_api, mimetype, data, true)
      |> Map.put("thumbnail_info", thumbnail_info)

    Map.put(msg, "info", info)
  end

  defp build_attachment_data(msg, client_api, mimetype, data, thumbnail \\ false) do
    key = if thumbnail == true, do: "thumbnail_url", else: "url"

    case data do
      {:file, _path} ->
        case Polyjuice.Client.Media.upload(
               client_api,
               data,
               mimetype: mimetype
             ) do
          {:ok, url} ->
            Map.put(msg, key, url)

          err ->
            raise err
        end

      {:url, url} ->
        Map.put(msg, key, to_string(url))

      {:data, data, filename} ->
        case Polyjuice.Client.Media.upload(
               client_api,
               data,
               mimetype: mimetype,
               filename: filename
             ) do
          {:ok, url} ->
            Map.put(msg, key, url)

          err ->
            raise err
        end

      _ ->
        raise('Not implemented')
    end
  end

  @doc """
  Download the file from an attachment object.

  Returns the same format as `Polyjuice.Client.Media.download`: if successful,
  returns a tuple of the form `{:ok, filename, content_type, body}`, where
  `body` is a `Stream` such that `Enum.join(body)` is the file contents.

  """
  @spec download(
          client_api :: Polyjuice.Client.API.t(),
          msg :: attachment()
        ) :: {:ok, String.t(), String.t(), Enumerable.t()} | any
  def download(client, %{"url" => url} = msg) do
    with {:ok, filename, content_type, body} <-
           Polyjuice.Client.Media.download(client, url, Map.get(msg, "filename")) do
      content_type =
        msg
        |> Map.get("info", %{})
        |> Map.get("mimetype", content_type)

      {:ok, filename, content_type, body}
    end
  end

  def download(_client, _msg) do
    {:error, :invalid_message}
  end

  @doc """
  Download the thumbnail from an attachment object.

  Returns the same format as `Polyjuice.Client.Media.download`: if successful,
  returns a tuple of the form `{:ok, filename, content_type, body}`, where
  `body` is a `Stream` such that `Enum.join(body)` is the file contents.

  """
  @spec download_thumbnail(
          client_api :: Polyjuice.Client.API.t(),
          msg :: attachment()
        ) :: {:ok, String.t(), String.t(), Enumerable.t()} | any
  def download_thumbnail(client, %{"info" => %{"thumbnail_url" => url}} = msg) do
    with {:ok, filename, content_type, body} <-
           Polyjuice.Client.Media.download(client, url, Map.get(msg, "filename")) do
      content_type =
        msg
        |> Map.get("info", %{})
        |> Map.get("thumbnail_info", %{})
        |> Map.get("mimetype", content_type)

      {:ok, filename, content_type, body}
    end
  end

  def download_thumbnail(_client, _msg) do
    {:error, :no_thumbnail}
  end

  defp set_info_key(info, _key, nil), do: info
  defp set_info_key(info, key, value), do: Map.put(info, key, value)

  # get the size of the given data parameter, if possible
  defp get_data_size({:file, filename}) do
    with {:ok, %{size: size}} <- File.stat(filename) do
      size
    else
      _ -> nil
    end
  end

  defp get_data_size({:data, data, _}) when is_binary(data), do: byte_size(data)
  defp get_data_size(_), do: nil

  defp get_filename({:file, filename}), do: Path.basename(filename)
  defp get_filename({:data, _, filename}), do: filename
  defp get_filename(_), do: "Attachment"
end
